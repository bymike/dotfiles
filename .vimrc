set nocompatible " required for vundle
filetype off     " required for vundle

syntax enable
set number

set expandtab
set tabstop=2
set shiftwidth=2

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'vim-airline/vim-airline'
Plugin 'CtrlP.vim'
Plugin 'flazz/vim-colorschemes'

call vundle#end()
filetype plugin indent on  " required for vundle

colorscheme monokai
