# Only run if shell is connected to a terminal
[ -t 1 ] || return

# path settings
GIT=/usr/local/git/bin
HOMEBREW=/usr/local/bin:/usr/local/lib:/usr/local/sbin
RBENV=$HOME/.rbenv/bin
RBENV_SHIMS=$HOME/.rbenv/shims
DIFF=~/dotfiles/code/icdiff
# COREUTILS=$(brew --prefix coreutils)/libexec/gnubin
export PATH=$GIT:$HOMEBREW:$RBENV:$RBENV_SHIMS:$DIFF:~/bin:$PATH

# loads dotfiles into shell
# ~/.extra used for settings I don’t want to commit
for file in ~/.{bash_prompt,aliases,functions,exports,extra}; do
  [ -r "$file" ] && source "$file"
done
unset file

# init rbenv
if which rbenv > /dev/null; then
  eval "$(rbenv init -)"
fi

export JRUBY_OPTS="-J-Xms1024m -J-Xmx4096m"

# tab completion for Git
. ~/dotfiles/code/git-completion.bash

# tab completion for homebrew
#. ~/dotfiles/code/homebrew-completion.sh

# tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
#[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2)" scp sftp ssh

# killall tab completion for common apps
#complete -o "nospace" -W "Contacts Calendar Dock Finder Mail Safari iTunes SystemUIServer Terminal Twitter" killall

# if possible, add tab completion for many more commands
[ -f /etc/bash_completion ] && source /etc/bash_completion

# sets gitconfig email based on computer in use (work v. home)
# if [ ${HOME} == "/Users/mdeutsch" ]; then
#  git config --global user.email michael.deutsch@veracross.com
# else
#   git config --global user.email michael@bymike.com
# fi

export NVM_DIR="/Users/mdeutsch/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

eval "$(nodenv init -)"

export GOPATH=${HOME}/src/go_ext:${HOME}/src/go
export PATH=${PATH}:/usr/local/go/bin:${HOME}/src/go_ext/bin:${HOME}/src/go/bin

export PATH=/usr/local/opt/python/libexec/bin:${HOME}/Library/Python/3.7/bin:${PATH}

# Veracross-specific
if [ -d ${HOME}/.veracross ]; then
  complete -C /Users/mdeutsch/.veracross/vcli-shell-completion vcli
fi

export VAULT_ADDR=https://vault-us-east-1.veracross.net:8200

# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[ -f /Users/mdeutsch/.nvm/versions/node/v8.12.0/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.bash ] && . /Users/mdeutsch/.nvm/versions/node/v8.12.0/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.bash
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[ -f /Users/mdeutsch/.nvm/versions/node/v8.12.0/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.bash ] && . /Users/mdeutsch/.nvm/versions/node/v8.12.0/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.bash

export PATH="/Users/mdeutsch/bin/Sencha/Cmd:$PATH"
