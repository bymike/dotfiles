#!/usr/bin/env bash
############################
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
############################

########## Variables

# dotfiles directory
dir=~/dotfiles

# old dotfiles backup directory
olddir=~/dotfiles_backup

# list of files/folders to symlink in homedir
files=".aliases .bash_profile .bash_prompt .bashrc .eslintrc .exports .extra .functions .gemrc .gitconfig .gitignore .gitmessage.txt .inputrc .irbrc .pryrc .vimrc .wgetrc"

##########

# create dotfilesBackup in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# # change to the dotfiles directory
# echo "Changing to the $dir directory"
# cd $dir
# echo "...done"

# move any existing dotfiles in homedir to dotfilesBackup directory, then create symlinks from the homedir to any files in the ~/dotfiles directory specified in $files
echo "Moving any existing dotfiles from ~ to $olddir & Creating symlink to $files in home directory."
for file in $files; do
    mv ~/$file $olddir
    ln -s $dir/$file ~/$file
    echo "symlinking $dir/$file -> ~/$file"
done

# install fonts
echo "Installing fonts"
rsync --exclude ".DS_Store" -av --no-perms ~/dotfiles/fonts/ ~/Library/Fonts/
echo "...done"

# Set up vim
echo "Setting up Vim"
# curl https://raw.githubusercontent.com/Shougo/neobundle.vim/master/bin/install.sh | sh
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
echo "...done"

# # Set up homebrew and apps
# echo "Installing binaries and apps with Homebrew"
# source ./setup/homebrew.sh
# echo "...done"

# # Set up Node environment
# echo "Installing node and global modules"
# source ./setup/node.sh
# echo "...done"

# # Set up Ruby environment
# echo "Setting up Ruby, Bundler & Rake"
# source ./setup/ruby.sh
# echo "...done"

# Set up Sublime Text environment
echo "Setting up Sublime Text"
source ./setup/sublime.sh
echo "...done"

# Set up OS X
echo "Setting up OS X"
source ./setup/osx.sh
echo "...done"

# Set up other stuff
echo "Setting git user name and email"
git config --global user.email michael.deutsch@veracross.com
