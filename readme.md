# Michael's dotfiles

Copied from https://github.com/okize/dotfiles

## Installation

```bash
cd ~ && git clone https://github.com/mdeutsch/dotfiles.git && cd dotfiles && source bootstrap.sh
```
