#!/usr/bin/env bash

# Check for Homebrew & install if necessary
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Make sure we’re using the latest Homebrew
brew update

# Upgrade any already-installed formulae
brew upgrade

echo "installing binaries..."

# Install GNU core utilities (those that come with OS X are outdated)
brew install coreutils

# Install GNU `find`, `locate`, `updatedb`, and `xargs`, g-prefixed
brew install findutils

# Install Bash 4
brew install bash

# Install more recent versions of some OS X tools
brew tap homebrew/dupes
brew install homebrew/dupes/grep

# Install wget with IRI support
brew install wget --enable-iri

# Install other useful binaries
binaries=(
  ack
  dockutil
  elasticsearch
  git
  git-cal
  graphicsmagick
  hub
  memcached
  mongodb
  mysql
  nvm
  optipng
  phantomjs
  postgresql
  python3
  rbenv
  redis
  rename
  Rserve
  ruby-build
  slack
  sqlite
  tmux
  trash
  tree
  webkit2png
)

brew install ${binaries[@]}

brew tap phinze/homebrew-cask
brew tap caskroom/versions
brew install brew-cask

function installcask() {
  brew cask install --appdir="/Applications" "${@}" 2> /dev/null
}

echo "installing applications..."

# Install native applications
apps=(
  appcleaner
  dropbox
  firefox
  flux
  google-chrome
  google-chrome-canary
  iterm2
  selfcontrol
  sublime-text3
  xquartz
  virtualbox
)

installcask ${apps[@]}

# Remove outdated versions from the cellar
brew cleanup

# Check for any problems
brew doctor
