To back up settings:

cp -R "$HOME/Library/Application Support/Code/User" vscode/User

To restore settings:

cp -R vscode/User "$HOME/Library/Application Support/Code/User"
