# Pry.config.editor = 'subl -n -w'

# Custom prompt
# prompt = "#{RUBY_VERSION}p#{RUBY_PATCHLEVEL}"
# Pry.config.prompt_name = prompt

Pry.config.prompt = [
  proc { |target_self, nest_level, pry|
    "(#{Pry.view_clip(target_self)})#{":#{nest_level}" unless nest_level.zero?}> "
  },

  proc { |target_self, nest_level, pry|
    "(#{Pry.view_clip(target_self)})#{":#{nest_level}" unless nest_level.zero?}* "
  }
]

=begin
# Pry::DEFAULT_PROMPT is:
Pry.config.prompt = [
	proc { |target_self, nest_level, pry|
		"[#{pry.input_array.size}] #{pry.config.prompt_name}(#{Pry.view_clip(target_self)})#{":#{nest_level}" unless nest_level.zero?}> "
	},

	proc { |target_self, nest_level, pry|
		"[#{pry.input_array.size}] #{pry.config.prompt_name}(#{Pry.view_clip(target_self)})#{":#{nest_level}" unless nest_level.zero?}* "
	}
]
=end

# # https://github.com/michaeldv/awesome_print/
# # $ gem install awesome_print
# begin
#   require 'awesome_print'
#   Pry.config.print = proc { |output, value| output.puts value.ai(:indent => 2) }
# rescue LoadError => e
#   warn "[WARN] #{e}"
#   puts '$ gem install awesome_print'
# end
